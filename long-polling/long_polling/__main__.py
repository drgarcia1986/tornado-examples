#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.ioloop
import tornado.httpserver
from tornado.options import define, options, parse_command_line
import os


class MessageController():
    def __init__(self):
        self.listeners = []

    def add_listener(self, callback):
        self.listeners.append(callback)

    def notify_listeners(self, msg):
        for callback in self.listeners:
            callback(msg)
        self.listeners.clear()


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class MessageHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.msg_controller = self.settings['message_controller']

    @tornado.web.asynchronous
    def get(self):
        self.msg_controller.add_listener(self.on_message)

    def on_message(self, msg):
        self.write({'message': msg})
        self.finish()

    def post(self):
        msg = self.get_argument('msg', None)
        if msg:
            self.msg_controller.notify_listeners(msg)
        self.finish()


if __name__ == "__main__":
    define('port', 8888, int, 'port of http server')
    parse_command_line()

    app = tornado.web.Application(
        handlers=[(r'/', MainHandler), (r'/message', MessageHandler)],
        message_controller=MessageController(),
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()