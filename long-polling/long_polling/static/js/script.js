$(document).ready(function() {
    setTimeout(listen_msg, 100);

    $('#msg-sender').click(function() {
        var user_msg = $('#msg').val();
        $.post('//localhost:8888/message',
            {msg: user_msg},
            function(data) {},
            "html");
        return false;
    });

    function listen_msg() {
        $.getJSON('//localhost:8888/message',
            function(data) {
                $('#msg-listen').append('<p>' + data.message + '</p>');
                setTimeout(listen_msg, 100);
            });
    };
});