# encoding: utf-8
import argparse
import importlib
import os


class _Settings(object):
    def __init__(self):
        self._setup()

    def _get_settings_from_cmd_line(self):
        parser = argparse.ArgumentParser()
        parser.add_argument('--settings', dest='settings')
        return parser.parse_args().settings

    def _setup(self):
        self._settings = self._get_settings_from_cmd_line()
        if self._settings is None:
            self._settings = os.environ.get('settings')
        if self._settings is None:
            raise RuntimeError('Settings are not configured')
        self._load_settings_module(self._settings)

    def _load_settings_module(self, settings_module):
        module = importlib.import_module(settings_module)
        for setting in dir(module):
            value = os.environ.get(setting, getattr(module, setting))
            setattr(self, setting, value)


settings = _Settings()
