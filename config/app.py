# encoding: utf-8

from tornado.web import Application, RequestHandler
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop

from framework.settings import settings


class ConfigHandler(RequestHandler):
    def get(self):
        self.finish({
            'example_title': settings.EXAMPLE_TITLE,
            'enviroment': settings.ENVIROMENT
        })


if __name__ == '__main__':
    app = Application(
        handles=[(r'/', ConfigHandler)],
        debug=settings.DEBUG
    )
    server = HTTPServer(app)
    server.listen(8080)
    IOLoop.instance().start()
