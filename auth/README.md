#Tornado "authenticated" example

This is a simple example of tornado decorator _tornado.web.authenticated_ in action.

The index page redirect to login page if method _get_current_user_ returns **None**.
