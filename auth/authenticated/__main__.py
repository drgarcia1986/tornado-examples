#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.httpserver
import tornado.ioloop
import sqlite3
import uuid
import json
import fakeredis
import os


class BaseHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.cursor = self.settings['cursor']
        self.session = self.settings['session']

    def get_current_user(self):
        session_id = self.get_argument('session', None)
        if session_id:
            session = self.session.get(session_id)
            return json.loads(session.decode()) if session else None
        return None


class LoginHandler(BaseHandler):
    def post(self):
        user = self.get_argument('user')
        password = self.get_argument('password')
        self.cursor.execute('SELECT * FROM users WHERE user=? and password=?', (user, password))
        user_info = self.cursor.fetchone()
        if user_info:
            session_id = str(uuid.uuid4())
            session_info = json.dumps({"user": user_info['user'], "name": user_info['name']})
            self.session.set(session_id, session_info)
            self.redirect("/?session={}".format(session_id))
        else:
            self.render("login.html", msg="Wrong user or password")

    def get(self):
        self.render("login.html", msg=None)


class IndexHandler(BaseHandler):
    @tornado.web.authenticated
    def get(self):
        self.render("index.html", user=self.current_user)


if __name__ == "__main__":
    conn = sqlite3.connect(":memory:")
    conn.row_factory = sqlite3.Row
    cursor = conn.cursor()
    cursor.execute("CREATE TABLE users (user TEXT, password TEXT, name TEXT)")
    cursor.execute("INSERT INTO users VALUES ('foo', 'secret', 'Foo')")
    cursor.execute("INSERT INTO users VALUES ('bar', 'security', 'Bar')")

    session = fakeredis.FakeRedis()

    app = tornado.web.Application(handlers=[(r'/login', LoginHandler), (r'/', IndexHandler)],
                                  cursor=cursor,
                                  session=session,
                                  login_url="/login",
                                  template_path=os.path.join(os.path.dirname(__file__), "templates"))
    server = tornado.httpserver.HTTPServer(app)
    server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
