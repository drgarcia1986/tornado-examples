#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.httpserver
import tornado.ioloop
from tornado.options import options, define, parse_command_line
from handlers import *
from controller import ChatController


class ChatApplication(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r'/', IndexHandler),
            (r'/chat', ChatPageHandler),
            (r'/chat_ws', ChatWebSocketHandler)
        ]
        settings = {
            'template_path': 'templates',
            'static_path': 'static',
            'cookie_secret': '9ee6045c8240f880f2da367fc2bf08ed'
        }
        self.chat_controller = ChatController()
        tornado.web.Application.__init__(self, handlers, **settings)


if __name__ == "__main__":
    define('port', 8888, int, "Default port for application")
    parse_command_line()

    app = ChatApplication()
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)

    tornado.ioloop.IOLoop.instance().start()
