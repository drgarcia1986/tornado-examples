if (!String.prototype.format) {
    String.prototype.format = function() {
        var formatted = this;
        for (var i = 0; i < arguments.length; i++) {
            var regexp = new RegExp('\\{'+i+'\\}', 'gi');
            formatted = formatted.replace(regexp, arguments[i]);
        }
        return formatted;
    }
};

function sendMessage(ipt, ws){
    var user_msg = ipt.val();
    ws.send(user_msg);
    ipt.val('');
};

$(document).ready(function() {
    var host = 'ws://192.168.0.107:8888/chat_ws';
    var websocket = new WebSocket(host);

    websocket.onopen = function(evt) { };
    websocket.onerror = function (evt) { };
    websocket.onmessage = function(evt) {
        var msg = $.parseJSON(evt.data)['msg'];
        var text = '<p><strong>{0}</strong>: {1}</p>'.format(msg.from, msg.msg);
        $('#msg-listen').append(text);
        $('#msg-listen')[0].scrollTop = $('#msg-listen')[0].scrollHeight;
    };

    $('#msg-sender').click(function() {
        sendMessage($('#msg'), websocket);
    });

    $('#msg').keypress( function(event) {
        if (event.which == 13) {
            sendMessage($('#msg'), websocket);
        }
    });
});