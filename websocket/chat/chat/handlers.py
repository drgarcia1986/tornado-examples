#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.websocket


class ChatWebSocketHandler(tornado.websocket.WebSocketHandler):
    def initialize(self):
        self.chat_controller = self.application.chat_controller

    def guest_name(self):
        return self.get_secure_cookie('guest_name').decode()

    def callback(self, msg):
        self.write_message(msg)

    def open(self):
        name = self.guest_name()
        self.chat_controller.add_guest(name, self.callback)

    def on_close(self):
        self.chat_controller.remove_guest(self.guest_name())

    def on_message(self, message):
        self.chat_controller.send_broadcast(message, self.guest_name())

    def check_origin(self, origin):
        return True


class ChatPageHandler(tornado.web.RequestHandler):
    def post(self):
        guest_name = self.get_argument('guest_name')
        if not guest_name:
            self.redirect('/')
        self.set_secure_cookie('guest_name', guest_name)
        self.render('chat.html')


class IndexHandler(tornado.web.RequestHandler):
    def get(self):
        self.render('index.html')