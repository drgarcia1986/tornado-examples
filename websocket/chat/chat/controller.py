#!/usr/bin/env python
# -*- coding: utf-8 -*-


class ChatController():
    def __init__(self):
        self.guests = {}

    def add_guest(self, name, callback):
        self.guests[name] = callback
        self.send_broadcast('%s entered.' % name)

    def remove_guest(self, name):
        del self.guests[name]
        self.send_broadcast('%s leave.' % name)

    def send_broadcast(self, msg, author=None):
        data = {'from': author or 'Server', 'msg': msg}
        for name, callback in self.guests.items():
            callback({'msg': data})
