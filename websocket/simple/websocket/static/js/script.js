$(document).ready(function() {
    var host = 'ws://localhost:8888/message';
    var websocket = new WebSocket(host);

    websocket.onopen = function(evt) { };
    websocket.onerror = function (evt) { };
    websocket.onmessage = function(evt) {
        message = $.parseJSON(evt.data)['message'];
        $('#msg-listen').append('<p>' + message + '</p>');
    };

    $('#msg-sender').click(function() {
        var user_msg = $('#msg').val();
        websocket.send(user_msg);
        return false;
    });
});