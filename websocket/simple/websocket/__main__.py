#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.ioloop
import tornado.httpserver
import tornado.websocket
from tornado.options import define, options, parse_command_line
import os


class MessageController():
    def __init__(self):
        self.listeners = []

    def add_listener(self, callback):
        self.listeners.append(callback)

    def remove_listener(self, callback):
        self.listeners.remove(callback)

    def notify_listeners(self, msg):
        for callback in self.listeners:
            callback(msg)


class MainHandler(tornado.web.RequestHandler):
    def get(self):
        self.render("index.html")


class MessageHandler(tornado.websocket.WebSocketHandler):
    def initialize(self):
        self.message_controller = self.settings['message_controller']

    def open(self):
        self.message_controller.add_listener(self.callback)

    def on_close(self):
        self.message_controller.remove_listener(self.callback)

    def callback(self, msg):
        self.write_message({'message': msg})

    def on_message(self, message):
        self.message_controller.notify_listeners(message)


if __name__ == "__main__":
    define('port', 8888, int, 'port of http server')
    parse_command_line()

    app = tornado.web.Application(
        handlers=[(r'/', MainHandler), (r'/message', MessageHandler)],
        message_controller=MessageController(),
        template_path=os.path.join(os.path.dirname(__file__), "templates"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(options.port)
    tornado.ioloop.IOLoop.instance().start()
