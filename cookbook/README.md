# Cookbook

##Index

* [Asynchronous Sleep](#asynchronous-sleep)

### Asynchronous Sleep

```python
from tornado import gen
from tornado.web import RequestHandler

class MyHandler(RequestHandler):
   @gen.coroutine
    def get(self):
        yield gen.Task(IOLoop.instance().add_timeout, time.time() + 5)  # 5 seconds
        self.write("OK!")
        self.finish()
```
