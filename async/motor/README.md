#Motor Example

This is a simple example of using Motor library for asynchronous Mongodb access

Make a **POST** with parameter amount in _/_ to insert random records on mongodb

Make a **GET** in _/_ to get all registers.

The Handler **ReverseStringHandler** is just for testing whether the server is asynchronous.
