# !/usr/bin/env python
# -*- coding: utf-8 -*-


import tornado.web
import tornado.ioloop
import tornado.httpserver
import tornado.gen
import motor
import uuid
import random


names = ["Diego", "Garcia", "Rafael", "Daniel", "Magrini", "Bruno", "Luiz", "Francisco", "Nune",
         "Mario", "Guedes", "Julio", "João", "Maria"]


class MainHandler(tornado.web.RequestHandler):
    def initialize(self):
        self.db = self.settings['mongo']['tests_motor']

    @tornado.gen.coroutine
    def get(self):
        result = []
        cursor = self.db['big_infos'].find()
        while (yield cursor.fetch_next):
            doc = cursor.next_object()
            del doc["_id"]
            result.append(doc)
        self.write({"result": result})
        self.finish()

    @tornado.gen.coroutine
    def post(self):
        amount = self.get_argument('amount', 0)
        result = []
        for i in range(int(amount)):
            doc = {"uuid": str(uuid.uuid4()),
                   "nome": random.choice(names),
                   "sobrenome": random.choice(names)}
            result = yield self.db['big_infos'].insert(doc)
            if "_id" in result.keys():
                del result["_id"]
            result.append(result)
        self.write({"result": result})
        self.finish()


class ReverseStringHandler(tornado.web.RequestHandler):
    def get(self):
        text = self.get_argument('text', None)
        if text:
            self.write(text[::-1])
        self.finish()


if __name__ == "__main__":
    app = tornado.web.Application(handlers=[(r'/', MainHandler), (r'/reverse', ReverseStringHandler)],
                                  mongo=motor.MotorClient('localhost', 27017))
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8888)
    tornado.ioloop.IOLoop.instance().start()
