# !/usr/bin/env python
# -*- coding: utf-8 -*-

import tornado.web
import tornado.httpserver
import tornado.ioloop
import motor
import tornado.gen
from bson.objectid import ObjectId
import os


class ImageHandler(tornado.web.RequestHandler):
    def initialize(self, fs):
        self.fs = fs

    @tornado.gen.coroutine
    def get(self, img):
        image = yield self.fs.get(ObjectId(img))
        if not image:
            self.set_status(404)
            self.finish()
        content = yield image.read()
        self.set_header("Content-Type", image.content_type)
        self.write(content)
        self.finish()
   
    @tornado.gen.coroutine
    def post(self):
        image = self.request.files['files'][0]['body']
        img_type = self.request.files['files'][0]['content_type']
        img_id = yield self.fs.put(image, content_type=img_type)
        self.render("image.html", img_id=img_id)


class IndexHandler(tornado.web.RequestHandler):
    def initialize(self, db):
        self.db = db

    @tornado.gen.coroutine
    def get(self):
        images = []
        cursor = self.db['fs.files'].find()
        while (yield cursor.fetch_next):
            doc = cursor.next_object()
            images.append(str(doc["_id"]))
        self.render("index.html", images=images)


if "__main__" == __name__:
    db = motor.MotorClient()["image_server"]
    fs = motor.MotorGridFS(db)
    app = tornado.web.Application(handlers=[(r"/image", ImageHandler, dict(fs=fs)), (r"/image/(\w+)", ImageHandler, dict(fs=fs)), (r"/", IndexHandler, dict(db=db))], 
                                  template_path=os.path.join(os.path.dirname(__file__), "templates"),
                                  debug=True)
    httpserver = tornado.httpserver.HTTPServer(app)
    httpserver.listen(8888)
    tornado.ioloop.IOLoop.instance().start()

